﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayTrigger : MonoBehaviour
{
    public AudioSource soundSource;
    public AudioClip clipToPlay;
    public bool playOnce = true;
    private bool hasPlayed = false;

    private void OnTriggerEnter(Collider other)
    {
        if (playOnce == true)
        {
            if (hasPlayed == false)
            {
                SoundPlay();
                hasPlayed = true;
            }
        }

        else
        {
            SoundPlay();
        }
    }

    public void SoundPlay()
    {
        soundSource.clip = clipToPlay;
        soundSource.Play();
    }
}
